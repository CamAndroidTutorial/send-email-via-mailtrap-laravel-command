<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Mail to User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::raw('Hello, World', function ($message){
                $message->to('650cd06638-49ff17@inbox.mailtrap.io')
                ->cc(array('650cd06638-49ff17@inbox.mailtrap.io'))
                ->bcc(array('sedibi@directmail24.net'));
        });

        echo "Done!, Please check your inbox.";
    }
}
